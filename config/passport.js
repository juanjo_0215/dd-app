const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require ('../models/usuario');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new LocalStrategy(
    function (email, password, done){
        Usuario.findOne({email: email}, function (err, usuario){
            if (err) { return done(err); }
            if (!usuario) { return done(null, false, { message: 'Email no existente o incorrecto' }); }
            if (!usuario.validPassword(password)) return done(null, false, { message: 'Password incorrecto' });
            if (usuario.verificado == false) { return done(null, false, { message: 'Usuario no verificado. Reenvie el token' }); }

            return done(null, usuario);
        });
    }
));

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    console.log('Entra a Google OAuth');
    Usuario.findOrCreateByGoogle(profile, function (err, user) {
      return done(err, user);
    });
  }
));

passport.use('facebookToken', new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET,
    fbGraphVersion: 'v3.0'
  }, 
  function(accessToken, refreshToken, profile, done) {
    console.log('Profile: ' + profile);
    console.log('Access Token: ' + accessToken);
    console.log('Refresh Token: ' + refreshToken);
    Usuario.findOrCreateByFacebook(profile, function (err, user) {
        return done(null, user);
    });
  }
));

passport.serializeUser(function (user, callBack){
    callBack(null, user.id);
});

passport.deserializeUser(function (id, callBack){
    Usuario.findById(id, function (err, usuario){
        callBack(err, usuario);
    });
});

module.exports = passport;
