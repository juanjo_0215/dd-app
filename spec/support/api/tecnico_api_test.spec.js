var Tecnico = require('../../../models/tecnico');
var request = require('request');
var server = require('../../../bin/www');

var mongoose = require('mongoose');
var base_url ='http://localhost:3000/api/tecnicos';

describe ('Testing tecnicos', function() {
   
    beforeEach(function (done){ 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB Connection Error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
   });

   afterEach(function (done){
        Tecnico.deleteMany({}, function (err, success){
            if (err) console.log(err);
            done();
        });
   });

   describe ('GET TECNICOS', () => {
        it ('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var bodyTec = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(bodyTec.tecnico.length).toBe(0);
                done();
            });
        });
    });

    describe ('POST TECNICOS  /Create  /Update  /Delete', () => { //Create
        it ('Status 200', (done) => {

            var headers ={'content-type' : 'application/json'};
            var tec1 = `{
                "num_identidad": 123456,
                "nombres": "Alejandro",
                "apellidos": "Henao",
                "pais": "Brasil",
                "departamento": "Brasilia",
                "ciudad": "Brasilia",
                "direccion": "Carrera 7ma con 34",
                "barrio": "Centro",
                "telefono": 3455454554,
                "especialidad": "TVS",
                "lat": 4.642108,
                "lng": -74.090164
            }`;

            request.post({
                headers: headers,
                url: base_url + '/create',
                body: tec1
            }, function (error, response, body){
                expect(response.statusCode).toBe(200);
                var bodyTec = JSON.parse(body).tecnico;
                console.log(bodyTec);
                expect(bodyTec[0].pais).toBe('Brasil');
                expect(bodyTec[0].ciudad).toBe('Brasilia');
                done();
            });

        });

        it ('Status 200', (done) => { // Update
            
            var tec1 = Tecnico.createInstance(123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);

            Tecnico.add(tec1, function(err, newTec){
                if (err) console.log(err);

                var headers ={'content-type' : 'application/json'};
                var tec1_modified = `{
                    "num_identidad": 123,
                    "nombres": "Luis",
                    "apellidos": "Rodriguez",
                    "pais": "Brasil",
                    "departamento": "Brasilia",
                    "ciudad": "Brasilia",
                    "direccion": "Carrera 90 con 34",
                    "barrio": "Centro",
                    "telefono": 3455454554,
                    "especialidad": "Plomeria, TVS, Carros",
                    "lat": 4.642108,
                    "lng": -74.090164
                }`;

                request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: tec1_modified
                }, function (error, response, body){
                    expect(response.statusCode).toEqual(200);
                    var bodyTec = JSON.parse(body).tecnico;
                    console.log(bodyTec);
                    expect(bodyTec.pais).toBe('Brasil');
                    expect(bodyTec.ciudad).toBe('Brasilia');
                    expect(bodyTec.telefono).toBe(3455454554);
                    done();
                });
            });
        });

        it ('Status 200', (done) => { // Delete
            
            var tec1 = Tecnico.createInstance(123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);

            Tecnico.add(tec1, function(err, newTec){
                if (err) console.log(err);
                var headers ={'content-type' : 'application/json'};
                var tec1_eliminated = `{ 
                    "num_identidad": 123 
                }`;

                request.post({
                    headers: headers,
                    url: base_url + '/delete',
                    body: tec1_eliminated
                }, function (error, response, body){
                    expect(response.statusCode).toEqual(204);
                    done();
                });
            });
        });
    });


});

//////////////////////////////////////////////////

/* describe ('Tecnico API', () => {
    
    describe ('GET TECNICOS', () => {
        it ('Status 200', (done) => {
            expect(Tecnico.allTecs.length).toBe(0);

            //var tec1 = new Tecnico (123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);
            //Tecnico.add (tec1);

            request.get('http://localhost:3000/api/tecnicos', function (error, response) {
                expect(response.statusCode).toEqual(200);
                done();
            });

        });
    });

    describe ('POST TECNICOS  /Create  /Update  /Delete', () => { //Create
        it ('Status 200', (done) => {

            var headers ={'content-type' : 'application/json'};
            var tec2 = `{
                "num_identidad": 123456,
                "nombres": "Alejandro",
                "apellidos": "Henao",
                "pais": "Brasil",
                "departamento": "Brasilia",
                "ciudad": "Brasilia",
                "direccion": "Carrera 7ma con 34",
                "barrio": "Centro",
                "telefono": 3455454554,
                "especialidad": "TVS",
                "lat": 4.642108,
                "lng": -74.090164
            }`;

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/tecnicos/create',
                body: tec2
            }, function (error, response, body){
                expect(response.statusCode).toEqual(200);
                expect(Tecnico.findById(123456).ciudad).toBe('Brasilia');
                expect(Tecnico.allTecs.length).toBe(1);
                done();
            });

        });

        it ('Status 200', (done) => { // Update
            var headers ={'content-type' : 'application/json'};
            var tec2_modified = `{
                "num_identidad": 123456,
                "nombres": "Alejandro",
                "apellidos": "Henao Davila",
                "pais": "Brasil",
                "departamento": "Brasilia",
                "ciudad": "Brasilia",
                "direccion": "Carrera 7ma con 34",
                "barrio": "Centro",
                "telefono": 9547898787,
                "especialidad": "TVS",
                "lat": 4.642108,
                "lng": -74.090164
            }`;

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/tecnicos/update',
                body: tec2_modified
            }, function (error, response, body){
                expect(response.statusCode).toEqual(200);
                expect(Tecnico.findById(123456).telefono).toBe(9547898787);
                expect(Tecnico.allTecs.length).toBe(1);
                done();
            });
        });

        it ('Status 200', (done) => { // Delete
            var headers ={'content-type' : 'application/json'};
            var tec1_eliminated = `{ 
                "num_identidad": 123, 
            }`;
            var tec2_eliminated = `{ 
                "num_identidad": 123456 
            }`;

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/tecnicos/delete',
                body: tec2_eliminated
            }, function (error, response, body){
                expect(response.statusCode).toEqual(204);
                expect(Tecnico.allTecs.length).toBe(0); // Habian dos tecnicos (tec1 [GET TECNICOS] y tec2 [POST TECNICOS])
                done();
            });
        });
    });

});
 */