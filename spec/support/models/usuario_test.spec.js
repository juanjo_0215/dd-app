var mongoose = require('mongoose');
var Tecnico = require('../../../models/tecnico');
var Usuario = require('../../../models/usuario');
var Contrato = require('../../../models/contrato');

describe ('Testing usuarios', function (){

    beforeEach(function (done){ 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB Connection Error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
   });

   afterEach(function (done){
        Tecnico.deleteMany({}, function (err, success){
            if (err) console.log(err);
            Contrato.deleteMany({}, function (err, success){
                if (err) console.log(err);
                Usuario.deleteMany({}, function (err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });
   });

   describe ('Cuando un usuario contrata un tecnico', () => {
        it ('Debe existir el contrato', (done) => {
            const usuario = new Usuario({ nombre: 'Juan', direccion: 'Cra 23 45-54'});
            usuario.save();
            const tecnico = new Tecnico({
                num_identidad: 123, 
                nombres: 'Luis', 
                apellidos: 'Rodriguez', 
                pais: 'Colombia', 
                departamento: 'Cundinamarca', 
                ciudad: 'Bogota DC.', 
                direccion: 'Kennedy Bomberos', 
                barrio: 'Kennedy', 
                telefono: 3157898787, 
                especialidad: 'Plomeria', 
                ubicacion: [4.6273188, -74.0844552]});
            tecnico.save();

            var hoy = new Date();
            var manana = new Date();
            var hora = new Date();
            hora.getHours();
            manana.setDate(hoy.getDate()+1);
            Usuario.contratar(usuario.id, tecnico.id, hoy, manana, hora, function (err, contrato){
                Contrato.find({}).populate('tecnico').populate('usuario').exec(function (err, contratos){
                    console.log(contratos[0]);
                    expect(contratos.length).toBe(1);
                    expect(contratos[0].diasContrato()).toBe(2);
                    expect(contratos[0].tecnico.num_identidad).toBe(123);
                    expect(contratos[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
   });



});