var mongoose = require('mongoose');
var Tecnico = require ('../../../models/tecnico');

describe ('Testing tecnicos', function() {
   
    beforeEach(function (done){ 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB Connection Error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
   });

   afterEach(function (done){
        Tecnico.deleteMany({}, function (err, success){
            if (err) console.log(err);
            done();
        });
   });

   describe ('Tecnico.createInstance', () => {
        it ('Crear una instancia de Tecnico', () => {
            var tec1 = Tecnico.createInstance(123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);

            expect(tec1.num_identidad).toBe(123);
            expect(tec1.pais).toBe('Colombia');
            expect(tec1.departamento).toBe('Cundinamarca');
            expect(tec1.ubicacion[0]).toBe(4.6273188);
            expect(tec1.ubicacion[1]).toBe(-74.0844552);
        });
    });

    describe ('Tecnico.allTecs', () => {
        it ('Comienza vacia', (done) => {
            Tecnico.allTecs(function (err, tecnicos){
                expect(tecnicos.length).toBe(0);
                done();
            });
        });
    });

    describe ('Tecnico.add', () => {
        it ('Agregar solo un tecnico', (done) => {
            var tec1 = new Tecnico({
                num_identidad: 123, 
                nombres: 'Luis', 
                apellidos: 'Rodriguez', 
                pais: 'Colombia', 
                departamento: 'Cundinamarca', 
                ciudad: 'Bogota DC.', 
                direccion: 'Kennedy Bomberos', 
                barrio: 'Kennedy', 
                telefono: 3157898787, 
                especialidad: 'Plomeria', 
                ubicacion: [4.6273188, -74.0844552]});

            Tecnico.add(tec1, function(err, newTec){
                if (err) console.log(err);
                Tecnico.allTecs(function (err, tecnicos){
                    expect(tecnicos.length).toBe(1);
                    expect(tecnicos[0].num_identidad).toBe(tec1.num_identidad);
                    done();
                });
            });
        });
    });

    describe ('Tecnico.findByNId', () => {
        it ('Debe retornar el tecnico con ID 123', (done) => {
            Tecnico.allTecs(function(err, tecnicos){

                expect(tecnicos.length).toBe(0);

                var tec1 = new Tecnico({
                    num_identidad: 123, 
                    nombres: 'Luis', 
                    apellidos: 'Rodriguez', 
                    pais: 'Colombia', 
                    departamento: 'Cundinamarca', 
                    ciudad: 'Bogota DC.', 
                    direccion: 'Kennedy Bomberos', 
                    barrio: 'Kennedy', 
                    telefono: 3157898787, 
                    especialidad: 'Plomeria', 
                    ubicacion: [4.6273188, -74.0844552]});

                Tecnico.add(tec1, function(err, newTec){
                    if (err) console.log(err);

                    var tec2 = new Tecnico({
                        num_identidad: 12345, 
                        nombres: 'Pedro', 
                        apellidos: 'Martinez', 
                        pais: 'Colombia', 
                        departamento: 'Cundinamarca', 
                        ciudad: 'Bogota DC.', 
                        direccion: 'Centro ', 
                        barrio: 'Centro', 
                        telefono: 3161230967, 
                        especialidad: 'TVS', 
                        ubicacion: [4.7273188, -74.0944552]});

                    Tecnico.add(tec2, function(err, newTec){
                        if (err) console.log(err);
                        Tecnico.findByNId(123, function (err, targetTec){
                            expect(targetTec.num_identidad).toBe(tec1.num_identidad);
                            expect(targetTec.especialidad).toBe(tec1.especialidad);
                            expect(targetTec.barrio).toBe(tec1.barrio);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe ('Tecnico.removeByNId', () => {
        it ('Debe retornar la lista de tecnicos vacia', (done) => {
            Tecnico.allTecs(function(err, tecnicos){

                expect(tecnicos.length).toBe(0);

                var tec1 = new Tecnico({
                    num_identidad: 123, 
                    nombres: 'Luis', 
                    apellidos: 'Rodriguez', 
                    pais: 'Colombia', 
                    departamento: 'Cundinamarca', 
                    ciudad: 'Bogota DC.', 
                    direccion: 'Kennedy Bomberos', 
                    barrio: 'Kennedy', 
                    telefono: 3157898787, 
                    especialidad: 'Plomeria', 
                    ubicacion: [4.6273188, -74.0844552]});

                Tecnico.add(tec1, function(err, newTec){
                    if (err) console.log(err);

                    var tec2 = new Tecnico({
                        num_identidad: 12345, 
                        nombres: 'Pedro', 
                        apellidos: 'Martinez', 
                        pais: 'Colombia', 
                        departamento: 'Cundinamarca', 
                        ciudad: 'Bogota DC.', 
                        direccion: 'Centro ', 
                        barrio: 'Centro', 
                        telefono: 3161230967, 
                        especialidad: 'TVS', 
                        ubicacion: [4.7273188, -74.0944552]});

                    Tecnico.add(tec2, function(err, newTec){
                        if (err) console.log(err);
                        
                        Tecnico.allTecs(function(err, tecnicos){
                            expect(tecnicos.length).toBe(2);
                            Tecnico.removeByNId(123, function (err, targetTec){
                                if (err) console.log(err);
                                Tecnico.removeByNId(12345, function (err, targetTec){
                                    if (err) console.log(err);

                                    Tecnico.allTecs(function(err, tecnicos){
                                        expect(tecnicos.length).toBe(0);
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    

});





////////////////////////////////////////

/* beforeEach(() => { Tecnico.allTecs = []; });

describe ('Tecnico.allTecs', () => {
    it ('Comienza vacio', () => {
        expect(Tecnico.allTecs.length).toBe(0);
    });
});

describe ('Tecnico.add', () => {
    it ('Se agrega una', () => {
        expect(Tecnico.allTecs.length).toBe(0);

        var tec1 = new Tecnico (123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);
        Tecnico.add (tec1);

        expect(Tecnico.allTecs.length).toBe(1);
        expect(Tecnico.allTecs[0]).toBe(tec1);
        
    });
});

describe ('Tecnico.findById', () => {
    it ('Debe retornar el tecnico con la id 123', () => {
        expect(Tecnico.allTecs.length).toBe(0);

        var tec1 = new Tecnico (123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);
        var tec2 = new Tecnico (1234, 'Alejandro', 'Martinez Martinez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Carrera 7ma con 34', 'Centro', 3148972343, 'Electrodomesticos', [4.6373188, -74.0844552]);

        Tecnico.add (tec1);
        Tecnico.add (tec2);

        var targetTec = Tecnico.findById(123);
        expect(targetTec.num_identidad).toBe(123);
        expect(targetTec.especialidad).toBe('Plomeria');
        expect(targetTec.pais).toBe('Colombia');
    });
});

describe ('Tecnico.removeById', () => {
    it ('Debe retornar la lista de tecnicos vacia', () => {
        expect(Tecnico.allTecs.length).toBe(0);

        var tec1 = new Tecnico (123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);
        var tec2 = new Tecnico (1234, 'Alejandro', 'Martinez Martinez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Carrera 7ma con 34', 'Centro', 3148972343, 'Electrodomesticos', [4.6373188, -74.0844552]);

        Tecnico.add (tec1);
        Tecnico.add (tec2);

        Tecnico.removeById(Tecnico.findById(123).num_identidad);
        Tecnico.removeById(Tecnico.findById(1234).num_identidad);


        expect(Tecnico.allTecs.length).toBe(0);
    });
}); */