var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var contratoSchema = new Schema ({
    fecha_solicitud: Date,
    fecha_visita: Date,
    hora_visita: Date,
    tecnico: { type: mongoose.Schema.Types.ObjectId, ref: 'Tecnico' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' }
});

contratoSchema.methods.diasContrato = function (){
    return moment(this.fecha_visita).diff(moment(this.fecha_solicitud), 'days') + 1;
}

module.exports = mongoose.model('Contrato', contratoSchema);