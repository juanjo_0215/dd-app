var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tecnicoSchema = new mongoose.Schema({
    num_identidad: Number,
    nombres: String,
    apellidos: String,
    pais: String,
    departamento: String,
    ciudad: String,
    direccion: String,
    barrio: String,
    telefono: Number,
    especialidad: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

tecnicoSchema.statics.createInstance = function (num_identidad, nombres, apellidos, pais, departamento, ciudad, direccion, barrio, telefono, especialidad, ubicacion){
    return new this({
        num_identidad: num_identidad,
        nombres: nombres,
        apellidos: apellidos,
        pais: pais,
        departamento: departamento,
        ciudad: ciudad,
        direccion: direccion,
        barrio: barrio,
        telefono: telefono,
        especialidad: especialidad,
        ubicacion: ubicacion
    });
};

tecnicoSchema.methods.toString = function (){
    return 'code: ' + this.num_identidad + ' Nombres: ' + this.nombres + ' | Apellidos: ' + this.apellidos;
};

tecnicoSchema.statics.allTecs = function (callBack){
    return this.find({}, callBack);
};

tecnicoSchema.statics.add = function (newTec, callBack){
    this.create(newTec, callBack);
};

tecnicoSchema.statics.findByNId = function (num_id, callBack){
    return this.findOne({num_identidad: num_id}, callBack);
};

tecnicoSchema.statics.updateByNId = function (num_id, doc_modified, callBack){
    return this.updateOne({num_identidad: num_id}, doc_modified, callBack);
};

tecnicoSchema.statics.removeByNId = function (num_id, callBack){
    return this.deleteOne({num_identidad: num_id}, callBack);
};

tecnicoSchema.statics.removeAll = function (){
    Tecnico.deleteMany({}, function (err, success){
        if (err) console.log(err);
    });
};

module.exports = mongoose.model('Tecnico', tecnicoSchema);

////////////////////////////////////////

/* var Tecnico = function (num_identidad, nombres, apellidos, pais, departamento, ciudad, direccion, barrio, telefono, especialidad, ubicacion) {
    this.num_identidad = num_identidad;
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.pais = pais;
    this.departamento = departamento;
    this.ciudad = ciudad;
    this.direccion = direccion;
    this.barrio = barrio;
    this.telefono = telefono;
    this.especialidad = especialidad;
    this.ubicacion = ubicacion;
}

Tecnico.prototype.toString = function (){
    return 'Numero de identidad: ' + this.num_identidad + ' | Nombres: ' + this.nombres + ' | Apellidos: ' + this.apellidos;
}

Tecnico.allTecs = [];

Tecnico.add = function (tecnico){
    Tecnico.allTecs.push(tecnico);
}

Tecnico.findById = function (tecId){
    var sTec = Tecnico.allTecs.find(x => x.num_identidad == tecId);
    if (sTec)
        return sTec;
    else
        throw new Error(`No existe un técnico con el número de identificacion: ${tecId}`);
}

Tecnico.removeById = function (tecId){
    for(var i = 0; i < Tecnico.allTecs.length; i++){
        if (Tecnico.allTecs[i].num_identidad == tecId){
            Tecnico.allTecs.splice(i, 1);
            break;
        }
    }
}

var tec1 = new Tecnico (123, 'Luis', 'Rodriguez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Kennedy Bomberos', 'Kennedy', 3157898787, 'Plomeria', [4.6273188, -74.0844552]);
var tec2 = new Tecnico (1234, 'Alejandro', 'Martinez Martinez', 'Colombia', 'Cundinamarca', 'Bogota DC.', 'Carrera 7ma con 34', 'Centro', 3148972343, 'Electrodomesticos', [4.6373188, -74.0844552]);

//Tecnico.add (tec1);
//Tecnico.add (tec2);

module.exports = Tecnico; */