var mongoose = require('mongoose');
var Contrato = require('./contrato');
var Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');
const sendgrid = require('@sendgrid/mail');

var validateEmail = function (email){

    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
    
};

var usuarioSchema = new Schema ({
    nombreUsuario: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {
    message: 'el {PATH} ya existe con otro usuario'
});

usuarioSchema.pre('save', function(next){
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.statics.contratar = function(user_id, tec_id, fecha_solicitud, fecha_visita, hora_visita, callBack){
    var contrato = new Contrato({
        usuario: user_id, 
        tecnico: tec_id, 
        fecha_solicitud: fecha_solicitud, 
        fecha_visita: fecha_visita, 
        hora_visita: hora_visita});
    console.log(contrato);
    contrato.save(callBack);
}

usuarioSchema.methods.enviar_email_bienvenida = function (callBack){
    const token = new Token ({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err){
        if (err) { return console.log(err.message); }

        var mailOptions;

        sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

        if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging'){

            mailOptions = {
                to: email_destination,
                from: 'juanjo_0215@hotmail.com',
                subject: 'Verificacion de cuenta',
                html: '<p>Hola:</p><br>' + '<p>Por favor, para verificar la cuenta haga click en el siguiente enlace:</p><br>' + '<a href=\"https://red-tecnicos.herokuapp.com' + '/token/confirmation/' + token.token + '\">Click aqui!</a>'
            };

            sendgrid.send(mailOptions).then(() => {
                console.log('Message sent');
            }).catch((error) => {
                console.log(error.response.body);
            })
        } else {

            mailOptions = {
                to: email_destination,
                from: 'no-reply@red-tecnicos.com',
                subject: 'Verificacion de cuenta',
                html: '<p>Hola:</p><br>' + '<p>Por favor, para verificar la cuenta haga click en el siguiente enlace:</p><br>' + '<a href=\"http://localhost:3000' + '/token/confirmation/' + token.token + '\">Click aqui!</a>'
            };

            mailer.sendMail(mailOptions, function (err){
                if (err) { return console.log(err.message); }
                console.log('A verification email has been sent to ' + email_destination);
            });
        }

    });
}

usuarioSchema.methods.resetPassword = function (callBack){
    const token = new Token ({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err){
        if (err) { return console.log(err.message); }

        var mailOptions;

        if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging'){
            
            mailOptions = {
                to: email_destination,
                from: 'juanjo_0215@hotmail.com',
                subject: 'Recuperacion de password de cuenta',
                html: '<p>Hola:</p><br>' + '<p>Por favor, para recuperar la cuenta haga click en el siguiente enlace:</p><br>' + '<a href=\"https://red-tecnicos.herokuapp.com' + '/resetPassword/' + token.token + '\">Click aqui!</a>'
            };

            sendgrid.send(mailOptions).then(() => {
                console.log('Message sent');
            }).catch((error) => {
                console.log(error.response.body);
            })
        
        } else {

            mailOptions = {
                to: email_destination,
                from: 'no-reply@red-tecs.com',
                subject: 'Recuperacion de password de cuenta',
                html: '<p>Hola:</p><br>' + '<p>Por favor, para recuperar la cuenta haga click en el siguiente enlace:</p><br>' + '<a href=\"http://localhost:3000' + '/resetPassword/' + token.token + '\">Click aqui!</a>'
            };

            mailer.sendMail(mailOptions, function (err){
                if (err) { return console.log(err.message); }
                console.log('A recover email has been sent to ' + email_destination);
            });

        }

        callBack(null);

    });
}

usuarioSchema.statics.findOrCreateByGoogle = function findOrCreate(condition, callBack) {
    const self = this;
    console.log("My Google profile" + condition);
    self.findOne({
        $or: [
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if (result) {
                callBack(err, result);
            } else {
                console.log('------------  CONDITION  -----------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombreUsuario = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex'); // Refactor - Crear objeto que represente auth por Google, FB o Local / mapear estrategias para no pasarle un password al azar
                console.log('------------  VALUES  -----------');
                console.log(values);
                self.create(values, (err, result)=> {
                    if (err) { console.log(err); }
                    return callBack(err, result);
                });
            }
    });
}

usuarioSchema.statics.findOrCreateByFacebook = function findOrCreateF(condition, callBack) {
    const self = this;
    console.log("My Facebook profile" + condition);
    self.findOne({
        $or: [
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if (result) {
                callBack(err, result);
            } else {
                console.log('------------  CONDITION  -----------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombreUsuario = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex'); //Objeto representado con la estrategia de login  Refactor - Crear objeto que represente auth por Google, FB o Local / mapear estrategias para no pasarle un password al azar
                console.log('------------  VALUES  -----------');
                console.log(values);
                self.create(values, (err, result)=> {
                    if (err) { console.log(err); }
                    return callBack(err, result);
                });
            }
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);