var express = require('express');
var router = express.Router();
var controllerToken = require('../controllers/token');

router.get('/confirmation/:token', controllerToken.confirmationGet);

module.exports = router;