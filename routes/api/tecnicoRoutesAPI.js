var express = require ('express');
var router = express.Router();
var tecnicoController = require('../../controllers/api/tecnicoControllerAPI');

router.get('/', tecnicoController.tecnico_list);
router.post('/create', tecnicoController.tecnico_create);
router.post('/update', tecnicoController.tecnicos_update);
router.post('/delete', tecnicoController.tecnico_delete);

module.exports = router;