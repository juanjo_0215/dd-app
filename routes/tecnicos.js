var express = require ('express');
var router = express.Router();
var tecnicoController = require('../controllers/tecnico');

router.get('/', tecnicoController.tecnicos_list);
router.get('/create', tecnicoController.tecnicos_create_get);
router.post('/create', tecnicoController.tecnicos_create_post);
router.get('/:num_identidad/detail', tecnicoController.tecnicos_detail_get);
router.get('/:num_identidad/update', tecnicoController.tecnicos_update_get);
router.post('/:num_identidad/update', tecnicoController.tecnicos_update_post);
router.post('/:num_identidad/delete', tecnicoController.tecnicos_delete_post);

module.exports = router;