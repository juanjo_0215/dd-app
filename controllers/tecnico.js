var Tecnico = require ('../models/tecnico.js');

exports.tecnicos_list = function (req, res) {

    Tecnico.allTecs(function (err, tecnicos){
        res.render('tecnicos/index', {tecs: tecnicos});
    });

}

exports.tecnicos_create_get = function(req, res){
    res.render('tecnicos/create');
}

exports.tecnicos_create_post = function(req, res){
    
    var tec = new Tecnico( { 
        num_identidad: req.body.num_identidad, 
        nombres: req.body.nombres, 
        apellidos:  req.body.apellidos, 
        pais: req.body.pais, 
        departamento:  req.body.departamento, 
        ciudad:  req.body.ciudad, 
        direccion:  req.body.direccion, 
        barrio: req.body.barrio, 
        telefono: req.body.telefono, 
        especialidad: req.body.especialidad, 
        ubicacion: [req.body.lat, req.body.lng]});
    
    Tecnico.add(tec, function(err, newTec){
        if (err) console.log(err);
        res.redirect('/tecnicos');
    });

    
}

exports.tecnicos_detail_get = function(req, res){
    
    Tecnico.findByNId(req.params.num_identidad, function (err, sTec){
        if (err) console.log(err);
        res.render('tecnicos/detail', {sTec});
    });

}

exports.tecnicos_update_get = function(req, res){

    Tecnico.findByNId(req.params.num_identidad, function (err, sTec){
        if (err) console.log(err);
        res.render('tecnicos/update', {sTec});
    });
    
}

exports.tecnicos_update_post = function(req, res){
    
    Tecnico.findByNId(req.params.num_identidad, function (err, sTec){
        if (err) console.log(err);
        
        var sTec = {
            num_identidad: req.body.num_identidad, 
            nombres: req.body.nombres, 
            apellidos: req.body.apellidos, 
            pais: req.body.pais, 
            departamento: req.body.departamento, 
            ciudad: req.body.ciudad, 
            direccion: req.body.direccion, 
            barrio: req.body.barrio,
            telefono: req.body.telefono, 
            especialidad: req.body.especialidad, 
            ubicacion: [req.body.lat, req.body.lng]};

        Tecnico.updateByNId(req.params.num_identidad, sTec, function(err, newTec){
            if (err) console.log(err);
            res.redirect('/tecnicos');
        });
    });

}

exports.tecnicos_delete_post = function(req, res){
    
    Tecnico.removeByNId(req.body.num_identidad, function(err, newTec){
        if (err) console.log(err);
        res.redirect('/tecnicos');
    });

}

