var Usuario = require ('../models/usuario');

exports.usuarios_list = function (req, res) {

    Usuario.find(function (err, usuarios){
        res.render('usuarios/index', {usuarios: usuarios});
    });

}

exports.usuarios_create_get = function(req, res){
    res.render('usuarios/create', {
        errors: {},
        usuario: new Usuario()
    });  
}

exports.usuarios_create_post = function(req, res){
    
    if (req.body.password != req.body.confirm_password){
        res.render('usuarios/create', {errors: {confirm_password: {message: 'No coincide el password'}}, usuario: new Usuario({ nombreUsuario: req.body.nombreUsuario, email: req.body.email})});
        return;
    }
    
    Usuario.create({
        nombreUsuario: req.body.nombreUsuario,
        email: req.body.email,
        password: req.body.password
    }, function (err, nuevoUsuario){
        if (err){
            res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({ nombreUsuario: req.body.nombreUsuario, email: req.body.email})})
        }else{
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios')
        }
    });
}

exports.usuarios_update_get = function(req, res){

    Usuario.findById(req.params.id, function (err, usuario){
        if (err) console.log(err);
        res.render('usuarios/update', {errors:{}, usuario: usuario});
    });
    
}

exports.usuarios_update_post = function(req, res){

    var update_values = {
        nombreUsuario: req.body.nombreUsuario
    };
    
    Usuario.findByIdAndUpdate(req.params.id, update_values, function (err, usuario){
        if (err) {
            console.log(err);
            res.render('usuarios/update', {
                errors: err.errors,
                usuario: new Usuario({
                    nombreUsuario: req.body.nombreUsuario,
                    email: req.body.email
                })
            });   
        }else{
            res.redirect('/usuarios');
            return;
        }
    });

}

exports.usuarios_delete_post = function(req, res){
    
    Usuario.findByIdAndDelete(req.body.id, function(err, usuario){
        if (err) console.log(err);
        res.redirect('/usuarios');
    });

}

