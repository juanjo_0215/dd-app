var Usuario = require('../../models/usuario');;

exports.usuario_list = function (req, res){
    Usuario.find({}, function (err, usuarios){
        res.status(200).json({
            usuarios: usuarios
        });
    });
}

exports.usuario_create = function (req, res){

    var usuario = new Usuario({
        nombreUsuario: req.body.nombre, 
        email:  req.body.email,
        password: req.body.password});
        
    usuario.save(function(err){
        if (err) console.log(err);
        usuario.enviar_email_bienvenida();
        res.status(200).json({
            usuarios: usuario
        });
    }); 
}

exports.usuario_contratar = function (req, res){

    Usuario.findById(req.body.id, function (err, usuario){
        console.log(usuario);
        Usuario.contratar(req.body.user_id, req.body.tec_id, req.body.fecha_solicitud, req.body.fecha_visita, req.body.hora_visita, function (err){
            console.log('Contrato!!!');
            res.status(200).send();
        });
    });
}