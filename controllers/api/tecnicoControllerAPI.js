var Tecnico = require('../../models/tecnico');
var mongoose = require('mongoose');

exports.tecnico_list = function (req, res){
    Tecnico.allTecs(function (err, tecnicos){
        res.status(200).json({
            tecnico: tecnicos
        });
    });
}

exports.tecnico_create = function (req, res){

    var tec1 = new Tecnico({
        num_identidad: req.body.num_identidad, 
        nombres: req.body.nombres, 
        apellidos:  req.body.apellidos, 
        pais: req.body.pais, 
        departamento:  req.body.departamento, 
        ciudad:  req.body.ciudad, 
        direccion:  req.body.direccion, 
        barrio: req.body.barrio, 
        telefono: req.body.telefono, 
        especialidad: req.body.especialidad, 
        ubicacion: [req.body.lat, req.body.lng]});
        
        Tecnico.add(tec1, function(err, newTec){
            if (err) console.log(err);
            Tecnico.allTecs(function (err, tecnicos){
                res.status(200).json({
                    tecnico: tecnicos
                });
            });
        });
        
}

exports.tecnicos_update = function(req, res){
    
    Tecnico.findByNId(req.body.num_identidad, function (err, targetTec){
        if (err) console.log(err);
                
        var sTec = {
            num_identidad: req.body.num_identidad, 
            nombres: req.body.nombres, 
            apellidos: req.body.apellidos, 
            pais: req.body.pais, 
            departamento: req.body.departamento, 
            ciudad: req.body.ciudad, 
            direccion: req.body.direccion, 
            barrio: req.body.barrio,
            telefono: req.body.telefono, 
            especialidad: req.body.especialidad, 
            ubicacion: [req.body.lat, req.body.lng]};

        Tecnico.updateByNId(req.body.num_identidad, sTec, function(err, newTec){
            if (err) console.log(err);
            res.status(200).json({
                tecnico: sTec
            });
        });
    });
    
}

exports.tecnico_delete = function (req, res){

    Tecnico.removeByNId(req.body.num_identidad, function(err, newTec){
        if (err) console.log(err);
        res.status(204).send();
    });

}