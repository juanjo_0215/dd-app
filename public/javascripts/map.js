var mymap = L.map('main_map').setView([4.6386259, -74.1178958], 11);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
}).addTo(mymap);

//L.marker([4.6373188, -74.0844552], 18).addTo(mymap);
L.marker([4.6673188, -74.1044552], 18).addTo(mymap);
//L.marker([4.6273188, -74.0844552], 18).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/tecnicos",
    success: function (result){
        console.log(result);
        result.tecnicos.forEach(function (tec){
            L.marker(tec.ubicacion, {title: tec.num_identidad}).addTo(mymap);
        });
    }   
});