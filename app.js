require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const MemoryStore = require('memorystore')(session);
const MongoDBStore = require('connect-mongodb-session')(session);
const Usuario = require('./models/usuario');
const Token = require('./models/token');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tecnicosRouter = require('./routes/tecnicos');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authAPIRouter = require('./routes/api/authAPIRouter');
var tecnicosAPIRouter = require('./routes/api/tecnicoRoutesAPI');
var usuariosAPIRouter = require('./routes/api/usuarioRouterAPI');

const store = new session.MemoryStore;

var app = express();

app.set('secretKey', 'jwt_pwd_!!2233445566');

var storeOptions;
if (process.env.NODE_ENV === 'development'){
  storeOptions = new MemoryStore({
    checkPeriod: 86400000  // prune expired entries every 24h
  });
} else {
  storeOptions = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  storeOptions.on('error', function(error) {
    console.log(error);
  });
}

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: storeOptions,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red-tecnicos_!!!@@!@??>>>>>1232...432UJI)*86%'
}));

// Mongoose config - MongoDB
var mongoose = require('mongoose');
const { assert } = require('console');

// var mongoDB = 'mongodb://localhost/red-tecnicos'; // Local - Usar en ambiente de desarrollo
// mongodb+srv://admin:<password>@cluster0.gwznh.mongodb.net/<dbname>?retryWrites=true&w=majority // Cloud - Usar a la salida en produccion
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('useCreateIndex', true); // Usar este porque el createCollection esta deprecado
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection Error'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// Configuracion Login y Session
app.get('/login', function (req, res){
  res.render('session/login');
});

app.post('/login', function (req, res, next){
  passport.authenticate('local', function (err, usuario, info){
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function (err){
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function (req, res){
  req.logOut();
  if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging'){
    res.redirect('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=https://red-tecnicos.herokuapp.com/'); // Buscar otra solucion (solucion temporal)
  } else {
    res.redirect('/');
  }
  
});

app.get('/forgotPassword', function (req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function (req, res){
  Usuario.findOne({ email: req.body.email }, function (err, usuario){
    if(!usuario) return res.render('session/forgotPassword', { info: { message: 'No existe el email para un usuario existente' } });
    usuario.resetPassword(function (err){
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });
    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function (req, res, next){
  Token.findOne({ token: req.params.token }, function (err, token){
    if(!token) return res.status(400).send({ msg: 'No existe un usuario asociado al token. Verifique el el token no haya expirado' });
    Usuario.findById(token._userId, function (err, usuario){
      if(!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token' });
      res.render('session/resetPassword', { errors: {}, usuario: usuario });
    });
  });
});

app.post('/resetPassword', function (req, res){
  if (req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', { errors: { confirm_password: { message: 'No coincide con el password ingresado' }}, usuario: new Usuario({ email: req.body.email })});
    return;
  }
  Usuario.findOne({ email: req.body.email }, function (err, usuario){
    usuario.password = req.body.password;
    usuario.save(function (err){
      if (err) {
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario ({ email: req.body.email })});
      }else {
        res.redirect('/login');
      }
    });
  });
});


// FIN Configuracion Login y Session

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/tecnicos', loggedIn, tecnicosRouter); // Ruta validada con autenticacion
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/api/auth', authAPIRouter);
app.use('/api/tecnicos', validarUsuario, tecnicosAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

app.use('/privacy_policy', function (req, res){
  res.redirect('/privacy_policy.html');
});

app.use('/google9e4aeb40f13eb125', function (req, res){
  res.redirect('/google9e4aeb40f13eb125.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email' ] }
));

app.get( '/auth/google/callback', 
    passport.authenticate( 'google', { 
        successRedirect: '/',
        failureRedirect: '/error'
}));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if (req.user) {
    next();
  }else {
    console.log('user sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify' + decoded);
      next();
    }
  });
}

module.exports = app;
